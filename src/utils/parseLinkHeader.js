const qs = require('querystring');
const url = require('url');
const xtend = require('xtend');

function hasRel(x) {
  return x && x.rel;
}

function intoRels(acc, x) {
  function splitRel(rel) {
    acc[rel] = xtend(x, { rel });
  }

  x.rel.split(/\s+/).forEach(splitRel);

  return acc;
}

function createObjects(acc, p) {
  // rel="next" => 1: rel 2: next
  const m = p.match(/\s*(.+)\s*=\s*"?([^"]+)"?/);
  // eslint-disable-next-line prefer-destructuring
  if (m) acc[m[1]] = m[2];
  return acc;
}

function parseLink(link) {
  try {
    const m = link.match(/<?([^>]*)>(.*)/);
    const linkUrl = m[1];
    const parts = m[2].split(';');
    const parsedUrl = url.parse(linkUrl);
    const qry = qs.parse(parsedUrl.query);

    parts.shift();

    let info = parts
      .reduce(createObjects, {});

    info = xtend(qry, info);
    info.url = linkUrl;
    return info;
  } catch (e) {
    return null;
  }
}

export default (linkHeader) => {
  if (!linkHeader) return null;

  return linkHeader.split(/,\s*</)
    .map(parseLink)
    .filter(hasRel)
    .reduce(intoRels, {});
};
