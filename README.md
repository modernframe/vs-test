# Videoslots Vue.js test
**Important notice** The responded `url` from the API was blocked the website to sameorigin so i couldn't put it in a modal iframe.
Hello! This is my first ever Vue.js application, i'm not really familiar with it, but i hope you gonna like it. Hope to here from you soon.

## Demo
You can reach the live demo at https://videoslots.modernframe.hu.

## GIT Repository and Download link
Public repository: [https://gitlab.com/modernframe/vs-test](https://gitlab.com/modernframe/vs-test)
Download link: [https://drive.google.com/file/d/1r628bi_S3zNZxxPSnVMt0wUnIb7s1WOm/view?usp=sharing](https://drive.google.com/file/d/1r628bi_S3zNZxxPSnVMt0wUnIb7s1WOm/view?usp=sharing)


## Files

Every components can be found at `src/components`.
Which components have no functionality, are placed at `src/components/UI`
I was using [Icomoon](https://icomoon.io/) icon font. The files are placed at `src/assets/icons`.
I used `scss` for styling, and placed every global style in separated files in folder `src/assets/scss`.

## Error handling

To test the error handling just simply try to rewrite the line in file `src/App.vue` at line `194` from:

    if (status !== 200) {
to

    if (status === 200) {
and check the results.
## Installing and running

Installing and running doesn't need any special requirements.

    $ npm i
    $ npm run serve
    $ open http://localhost:8080
## Features

 - SCSS
 - ES6
 - Icon fonts
 - SEO optimized
 - Lazy load images
 - Broken image handling
 - Performance optimization
 - Visible error handling
 - Full responsive
